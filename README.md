# android-hidl

This is an example for adding a new HIDL service to Android P.
For more information, it' recommanded to see https://blog.csdn.net/sinat_18179367/article/details/95940030


NOTE:

// Remember to update current.txt for VTS

hardware/interfaces$ hidl-gen -L hash -r android.hardware:hardware/interfaces -r android.hidl:system/libhidl/transport android.hardware.test@1.0 >> current.txt
